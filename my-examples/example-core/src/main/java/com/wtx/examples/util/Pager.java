package com.wtx.examples.util;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * <p>Description: web分页类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/8/16 14:05
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pager<T> implements Serializable
{
    private static final long serialVersionUID = 700756963442474836L;
    
    private static final int DEF_PGSIZE = 10;
    
    private int pageSize = DEF_PGSIZE;
    
    private int pageNo = 1;
    
    private int totalCount = 0;
    
    private List<T> list;
    
    public Pager()
    {
        init();
    }
    
    public Pager(int pageNo, int pageSize)
    {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        
        init();
    }
    
    public Pager(int totalCount, List<T> list)
    {
        this.totalCount = totalCount;
        this.list = list;
        
        init();
    }
    
    public Pager(int pageNo, int pageSize, int totalCount, List<T> list)
    {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.list = list;
        
        init();
    }
    
    private void init()
    {
        if (totalCount <= 0)
        {
            totalCount = 0;
        }
        if (pageSize <= 0)
        {
            pageSize = DEF_PGSIZE;
        }
        if (pageNo <= 0)
        {
            pageNo = 1;
        }
    }
    
    public List<T> getList()
    {
        return list;
    }
    
    public void setList(List<T> list)
    {
        this.list = list;
    }
    
    public int getPageNo()
    {
        return pageNo;
    }
    
    public int getPageSize()
    {
        return pageSize;
    }
    
    public int getTotalCount()
    {
        return totalCount;
    }
    
    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }
    
    public int getTotalPage()
    {
        int totalPage = totalCount / pageSize;
        if (totalCount % pageSize != 0 || totalPage == 0)
        {
            totalPage++;
        }
        return totalPage;
    }
    
    @JsonIgnore
    public int getOffset()
    {
        return (pageNo - 1) * pageSize;
    }
}