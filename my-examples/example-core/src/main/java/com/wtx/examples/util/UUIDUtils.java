package com.wtx.examples.util;

import java.util.Random;
import java.util.UUID;

/**
 * <p>Description: UUID工具类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/8/14 9:33
 */
public class UUIDUtils {
    public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };

    public static Random random = new Random();

    public static String generateShortUuid() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();

    }

    /**
     * 自动生成32位的UUid，对应数据库的主键id进行插入用。
     * @return
     */
    public static String getUUID() {

        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     *
     * @param min
     * @param max
     * @return
     */
    public static int getRandomIntInRange(int min,int max){
        return random.ints(min,(max+1)).limit(1).findFirst().getAsInt();
    }

    /**
     *
     * @param min
     * @param max
     * @return
     */
    public static String getRandomStringInRange(int min , int max){
        return String.valueOf(random.ints(min,(max + 1)).limit(1).findFirst().getAsInt());
    }
}
