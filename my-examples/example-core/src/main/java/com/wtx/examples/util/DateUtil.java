package com.wtx.examples.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.wtx.examples.constant.DicConstant;


/**
 * <p>Description: 日期工具类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/8/14 10:45
 */
public class DateUtil
{
    
    private static final ThreadLocal<SimpleDateFormat> SIMPLEDATEFORMAT = new ThreadLocal<>();
    
    private static final Object LOCK_FLAG = new Object();

    private static List<String> DATE_LIST;
    
    // 私有构造方法
    private DateUtil()
    {
    }
    
    /**
     * 获取SimpleDateFormat
     * 
     * @param pattern 日期格式
     * @return SimpleDateFormat对象
     * @throws RuntimeException 异常：非法日期格式
     */
    private static SimpleDateFormat getDateFormat(String pattern) throws RuntimeException
    {
        SimpleDateFormat dateFormat = SIMPLEDATEFORMAT.get();
        if (dateFormat == null)
        {
            synchronized (LOCK_FLAG)
            {
                if (dateFormat == null)
                {
                    dateFormat = new SimpleDateFormat(pattern);
                    dateFormat.setLenient(false);
                    SIMPLEDATEFORMAT.set(dateFormat);
                }
            }
        }
        dateFormat.applyPattern(pattern);
        return dateFormat;
    }
    
    /**
     * 获取日期中的某数值。如获取月份
     * 
     * @param date 日期
     * @param dateType 日期格式
     * @return 数值
     */
    private static int getInteger(Date date, int dateType)
    {
        int num = 0;
        Calendar calendar = Calendar.getInstance();
        if (date != null)
        {
            calendar.setTime(date);
            num = calendar.get(dateType);
        }
        return num;
    }
    
    /**
     * 增加日期中某类型的某数值。如增加日期
     * 
     * @param date 日期
     * @param dateType 类型
     * @param amount 数值
     * @return 计算后日期
     */
    private static Date addInteger(Date date, int dateType, int amount)
    {
        Date myDate = null;
        if (date != null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(dateType, amount);
            myDate = calendar.getTime();
        }
        return myDate;
    }
    
    /**
     * 将日期字符串转化为日期。失败返回null。
     *
     * @param date 日期字符串
     * @return 日期
     */
    public static Date stringToDate(String date)
    {
        Date myDate = null;
        if (date != null)
        {
            try
            {
                myDate = getDateFormat(DicConstant.DEFAULT_DATE_FORMAT).parse(date);
            }
            catch (Exception e)
            {
            }
        }
        return myDate;
    }
    
    /**
     * 将日期字符串转化为日期。失败返回null。
     * 
     * @param date 日期字符串
     * @param pattern 日期格式
     * @return 日期
     */
    public static Date stringToDate(String date, String pattern)
    {
        Date myDate = null;
        if (date != null)
        {
            try
            {
                myDate = getDateFormat(pattern).parse(date);
            }
            catch (Exception e)
            {
            }
        }
        return myDate;
    }
    
    /**
     * 将日期转化为日期字符串。失败返回null。
     *
     * @param date 日期
     * @return 日期字符串
     */
    public static String dateToString(Date date)
    {
        String dateString = null;
        if (date != null)
        {
            try
            {
                dateString = getDateFormat(DicConstant.DEFAULT_DATE_FORMAT).format(date);
            }
            catch (Exception e)
            {
            }
        }
        return dateString;
    }
    
    /**
     * 将日期转化为日期字符串。失败返回null。
     * 
     * @param date 日期
     * @param pattern 日期格式
     * @return 日期字符串
     */
    public static String dateToString(Date date, String pattern)
    {
        String dateString = null;
        if (date != null)
        {
            try
            {
                dateString = getDateFormat(pattern).format(date);
            }
            catch (Exception e)
            {
            }
        }
        return dateString;
    }
    
    /**
     * 将日期字符串转化为另一日期字符串。失败返回null。
     * 
     * @param date 旧日期字符串
     * @param olddPattern 旧日期格式
     * @param newPattern 新日期格式
     * @return 新日期字符串
     */
    public static String stringToString(String date, String olddPattern, String newPattern)
    {
        return dateToString(stringToDate(date, olddPattern), newPattern);
    }
    
    /**
     * 增加日期的年份。失败返回null。
     *
     * @param date 日期
     * @param monthAmount 增加数量。可为负数
     * @return 增加年份后的日期
     */
    public static Date addYear(Date date, int monthAmount)
    {
        return addInteger(date, Calendar.YEAR, monthAmount);
    }
    
    /**
     * 增加日期的月份。失败返回null。
     * 
     * @param date 日期
     * @param monthAmount 增加数量。可为负数
     * @return 增加月份后的日期
     */
    public static Date addMonth(Date date, int monthAmount)
    {
        return addInteger(date, Calendar.MONTH, monthAmount);
    }
    
    /**
     * 增加日期的天数。失败返回null。
     * 
     * @param date 日期
     * @param dayAmount 增加数量。可为负数
     * @return 增加天数后的日期
     */
    public static Date addDay(Date date, int dayAmount)
    {
        return addInteger(date, Calendar.DATE, dayAmount);
    }
    
    /**
     * 增加日期的小时。失败返回null。
     * 
     * @param date 日期
     * @param hourAmount 增加数量。可为负数
     * @return 增加小时后的日期
     */
    public static Date addHour(Date date, int hourAmount)
    {
        return addInteger(date, Calendar.HOUR_OF_DAY, hourAmount);
    }
    
    /**
     * 增加日期的分钟。失败返回null。
     * 
     * @param date 日期
     * @param minuteAmount 增加数量。可为负数
     * @return 增加分钟后的日期
     */
    public static Date addMinute(Date date, int minuteAmount)
    {
        return addInteger(date, Calendar.MINUTE, minuteAmount);
    }
    
    /**
     * 增加日期的秒钟。失败返回null。
     * 
     * @param date 日期
     * @param secondAmount 增加数量。可为负数
     * @return 增加秒钟后的日期
     */
    public static Date addSecond(Date date, int secondAmount)
    {
        return addInteger(date, Calendar.SECOND, secondAmount);
    }
    
    /**
     * 获取日期的年份。失败返回0。
     * 
     * @param date 日期
     * @return 年份
     */
    public static int getYear(Date date)
    {
        return getInteger(date, Calendar.YEAR);
    }
    
    /**
     * 获取日期的月份。失败返回0。
     * 
     * @param date 日期
     * @return 月份
     */
    public static int getMonth(Date date)
    {
        return getInteger(date, Calendar.MONTH) + 1;
    }
    
    /**
     * 获取日期的天数。失败返回0。
     * 
     * @param date 日期
     * @return 天
     */
    public static int getDay(Date date)
    {
        return getInteger(date, Calendar.DATE);
    }
    
    /**
     * 获取日期的小时。失败返回0。
     * 
     * @param date 日期
     * @return 小时
     */
    public static int getHour(Date date)
    {
        return getInteger(date, Calendar.HOUR_OF_DAY);
    }
    
    /**
     * 获取日期的分钟。失败返回0。
     * 
     * @param date 日期
     * @return 分钟
     */
    public static int getMinute(Date date)
    {
        return getInteger(date, Calendar.MINUTE);
    }
    
    /**
     * 获取日期的秒钟。失败返回0。
     * 
     * @param date 日期
     * @return 秒钟
     */
    public static int getSecond(Date date)
    {
        return getInteger(date, Calendar.SECOND);
    }
    
    public static int getIntervalHours(Date newTime, Date oldTime)
    {
        if (oldTime == null || newTime == null)
        {
            return 0;
        }
        int hours = 0;
        
        long time = newTime.getTime() - oldTime.getTime();
        if (time >= 0)
        {
            // 不足一小时按一小时算，计算尺度到毫秒
            hours = (int) ((time + 59 * 60 * 1000 + 59 * 1000 + 999) / (60 * 60 * 1000));
            return hours;
        }
        else
        {
            return -1;
        }
    }
    
    /**
     * 相差几分钟
     * 
     * @param newTime
     * @param oldTime
     * 
     */
    public static int getIntervalMinute(Date newTime, Date oldTime)
    {
        if (oldTime == null || newTime == null)
        {
            return 0;
        }
        
        long time = newTime.getTime() - oldTime.getTime();
        if (time >= 0)
        {
            // 不足一分钟按一分钟算，计算尺度到毫秒
            return (int) (time + 59 * 1000 + 999) / (60 * 1000);
        }
        else
        {
            return -1;
        }
    }
    
    /**
     * 相差多少秒
     * @param newTime
     * @param oldTime
     * @return
     */
    public static int getIntervalSecond(Date newTime, Date oldTime)
    {
        if (oldTime == null || newTime == null)
        {
            return 0;
        }
        
        long time = newTime.getTime() - oldTime.getTime();
        if (time >= 0)
        {
            // 相差的秒
            return (int) (time / 1000);
        }
        else
        {
            return 0;
        }
    }
    
    /**
     * 
     * 方法isSameDay的功能描述：判断两个日期是否同一天
     * 
     * @return  返回值
     */
    public static Boolean isSameDay(Date day1, Date day2)
    {
        if (null == day1 || null == day2)
        {
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2))
        {
            return true;
        }
        return false;
    }
    
    /**
     * 将日期转化为日期字符串。失败返回null。
     * 
     * @param date 日期
     * @param pattern 日期格式
     * @return 日期字符串
     */
    public static String DateToString(Date date, String pattern)
    {
        String dateString = null;
        if (date != null)
        {
            try
            {
                dateString = getDateFormat(pattern).format(date);
            }
            catch (Exception e)
            {
            }
        }
        return dateString;
    }

    /**
     * 获取当年的12个月日期对象字符串
     * @return List<Date>
     */
    public static List<String> getDateList()
    {
        if (DATE_LIST == null)
        {
            synchronized (DateUtil.class)
            {
                if (DATE_LIST == null)
                {
                    DATE_LIST = new ArrayList<>();
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    for (int i = 1; i < 13; i++) {
                        DATE_LIST.add(year + "年" + i + "月");
                    }
                }
            }
        }
        return DATE_LIST;
    }
}
