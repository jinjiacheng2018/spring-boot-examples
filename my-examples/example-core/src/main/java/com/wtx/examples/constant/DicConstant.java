package com.wtx.examples.constant;

/**
 * <p>Description: 数据字典常量类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/8/14 10:44
 */
public class DicConstant
{

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"; //默认时间格式


    public static final String TEST_MYTOPIC = "Test_MyTopic"; //topic信息

    public static final String TEST_TAGS = "TEST_TAGS"; //tags信息
}
