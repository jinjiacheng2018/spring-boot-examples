package com.wtx.examples.common;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * <p>Description: 返回信息封装类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/9/9 11:37:06
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultMessage<T>
{
    
    private String code;
    
    private T data;
    
    private String msg = "";
    
    private String exception;
    
    private Integer status = 1;
    
    public String getCode()
    {
        return code;
    }
    
    public void setCode(String code)
    {
        this.code = code;
    }
    
    public T getData()
    {
        return data;
    }
    
    public void setData(T data)
    {
        this.data = data;
    }
    
    public String getMsg()
    {
        return msg;
    }
    
    public void setMsg(String msg)
    {
        this.msg = msg;
    }
    
    public String getException()
    {
        return exception;
    }
    
    public void setException(String exception)
    {
        this.exception = exception;
    }
    
    public Integer getStatus()
    {
        return status;
    }
    
    public void setStatus(Integer status)
    {
        this.status = status;
    }
    
    public static ResultMessage message(String msg)
    {
        ResultMessage message = new ResultMessage();
        message.setMsg(msg);
        return message;
    }
    
    public static <T> ResultMessage<T> data(T data)
    {
        ResultMessage<T> message = new ResultMessage<T>();
        message.setData(data);
        return message;
    }
}
