package com.wtx.rocket.example.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.PostConstruct;

import com.wtx.rocket.example.util.SpringUtil;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.stereotype.Service;

import com.wtx.examples.constant.DicConstant;

@Service
public class RocketMQService {

    /**
     * 测试消费者方法
     */
    @PostConstruct
    public static void initConsumer() {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("ConsumerGroup");
        consumer.setNamesrvAddr("47.101.42.117:9876;101.132.135.181:9876");

        // 设置Consumer第一次启动是从队列头部开始消费还是队列尾部开始消费,如果非第一次启动，那么按照上次消费的位置继续消费
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        try {
            // 设置消费的topic,tags
            consumer.subscribe(DicConstant.TEST_MYTOPIC, "*");
        } catch (MQClientException e) {
            e.printStackTrace();
        }

        // 设置监听
        consumer.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    try {
                        String topic = msg.getTopic();
                        String msgId = msg.getMsgId();
                        String body = new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET);
                        System.err.println("【topic】" + topic + "\n【msgId】" + msgId + "\n【msg】" + body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                    }
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        try {
            // 启动消费者
            consumer.start();
            System.err.println("【消费者启动成功!!!】");
        } catch (MQClientException e) {
            e.printStackTrace();
            System.err.println("【消费者启动失败...】");
        }
    }

}
