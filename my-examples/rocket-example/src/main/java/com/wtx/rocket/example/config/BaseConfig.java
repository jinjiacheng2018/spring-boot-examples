package com.wtx.rocket.example.config;

import com.wtx.examples.constant.DicConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wtx.rocket.example.util.SpringUtil;
import org.springframework.context.annotation.Scope;

/**
 * <p>Description: 基础配置类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/9/9 13:15:31
 */
@Slf4j
@Configuration
public class BaseConfig {

    @Value("${rocketmq.producer.groupName}")
    private String groupName;

    @Value("${rocketmq.producer.namesrvAddr}")
    private String nameserAddr;

    @Value("${rocketmq.producer.maxMessageSize}")
    private int maxMessageSize;

    @Value("${rocketmq.producer.sendMsgTimeout}")
    private int sendMsgTimeout;

    @Value("${rocketmq.consumer.groupName}")
    private String consumerGroup;

    @Bean
    public SpringUtil getSpringUtil()
    {
        return new SpringUtil();
    }

    /**
     * 配置创建的bean是多例模式，以免关闭mq之后再次发送消息报错
     * @return DefaultMQProducer
     */
    @Bean(name = "producer")
    @Scope("prototype")
    public DefaultMQProducer producer()
    {
        DefaultMQProducer producer = new DefaultMQProducer(groupName);
        producer.setNamesrvAddr(nameserAddr);
        producer.setMaxMessageSize(maxMessageSize);
        producer.setSendMsgTimeout(sendMsgTimeout);
        producer.setVipChannelEnabled(false);

        try
        {
            producer.start();
            log.info("【RocketMQ生产者启动成功!!! nameserAddr:{},groupName:{}】", nameserAddr, groupName);
        }
        catch (MQClientException e)
        {
            log.error("【RocketMQ生产者启动失败】error info:{}" + e.getMessage());
            e.printStackTrace();
        }
        return producer;
    }

}
