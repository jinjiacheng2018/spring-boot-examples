package com.wtx.rocket.example.controller;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wtx.examples.common.ResultMessage;
import com.wtx.examples.constant.DicConstant;
import com.wtx.examples.util.UUIDUtils;
import com.wtx.rocket.example.util.SpringUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>Description: 描述信息 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/9/9 14:37:57
 */
@Slf4j
@RestController
@RequestMapping("/mq")
public class RocketMqController {

    /**
     * 发送消息，获取多实例的Producer，发送消息之后可以立即关闭
     * @return ResultMessage
     */
    @RequestMapping("/mq_sendInfo1")
    public ResultMessage mq_sendInfo1() throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        DefaultMQProducer producer = SpringUtil.getBean(DefaultMQProducer.class);
        // DefaultMQProducer producer = SpringUtil.getBean("producer");
        Message message = new Message(DicConstant.TEST_MYTOPIC,DicConstant.TEST_TAGS,UUIDUtils.getUUID(),"Hello,RocketMQ...".getBytes());
        SendResult send = producer.send(message);
        producer.shutdown();
        log.info("【RocketMQ生产者关闭成功!!!】");
        return ResultMessage.data(send);
    }

}
