package com.wtx.rocket.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketExampleApplication
{
    
    public static void main(String[] args)
    {
        SpringApplication.run(RocketExampleApplication.class, args);
    }
    
}
