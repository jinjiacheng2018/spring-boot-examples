package com.wtx.rocketmq.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoceketStarterExampleApplication
{
    
    public static void main(String[] args)
    {
        SpringApplication.run(RoceketStarterExampleApplication.class, args);
    }
    
}
