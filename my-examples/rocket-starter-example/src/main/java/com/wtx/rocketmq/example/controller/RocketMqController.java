package com.wtx.rocketmq.example.controller;

import com.alibaba.fastjson.JSONObject;
import com.wtx.examples.common.ResultMessage;
import com.wtx.examples.util.UUIDUtils;
import com.wtx.rocketmq.example.util.SpringUtil;
import com.zc.smartcity.rocketmq.core.RocketMQTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>Description: 发送消息 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/9/9 11:13:07
 */
@Slf4j
@RestController
@RequestMapping("/mq")
public class RocketMqController {

    // 配置自动注入
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 测试接口
     * @return JSONObject
     */
    @RequestMapping("/index")
    public JSONObject index()
    {
        log.info("【请求测试接口!!!】");
        JSONObject retVal = new JSONObject();
        retVal.put("title","Hello,RocketMQ!!!");
        return retVal;
    }

    /**
     * 使用RokcetMQTemplate发送消息
     * @return JSONObject
     */
    @RequestMapping("/sendMessage_1")
    public ResultMessage sendMessage_1()
    {
        log.info("【请求sendMessage_1接口,发送消息!!!】");
        rocketMQTemplate.sendOneWay("Test_MyTopic:TEST_TAGS","发送了一条消息");
        return ResultMessage.message("Hello,RocketMQ!!!");
    }

    /**
     * 使用DefaultMQProducer发送消息
     * @return JSONObject
     * @throws InterruptedException 异常
     * @throws RemotingException 异常
     * @throws MQClientException 异常
     * @throws MQBrokerException 异常
     */
    @RequestMapping("/sendMessage_2")
    public ResultMessage<SendResult> sendMessage_2() throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        log.info("【请求sendMessage_2接口,发送消息!!!】");
        DefaultMQProducer producer = rocketMQTemplate.getProducer();
        Message message = new Message("Test_MyTopic","TEST_TAGS",UUIDUtils.getUUID(),"Hello,RocketMQ...".getBytes());
        SendResult send = producer.send(message);
        return ResultMessage.data(send);
    }

}
