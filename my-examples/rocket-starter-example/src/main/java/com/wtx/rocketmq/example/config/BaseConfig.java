package com.wtx.rocketmq.example.config;

import com.wtx.rocketmq.example.util.SpringUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Description: 基础配置类 </p>
 * <p>Company: https://www.wtxjr.com </p>
 *
 *@author JinJiacheng
 *@version 2019/9/9 13:15:31
 */
@Configuration
public class BaseConfig {

    @Bean
    public SpringUtil getSpringUtil()
    {
        return new SpringUtil();
    }

}
